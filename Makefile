CC = gcc
CXX = clang++ -std=c++11
CFLAGS = -O3 -std=gnu99 -march=native -Wall -Wextra -pedantic

COMMON = splitmix64.c splitmix64.h xoshiro512starstar.c xoshiro512starstar.h

#SIMFLAGS?=-DMODE_DELTIME -DMID_AMOUNT=10 -DLAYERS=100 -DTRIX_D=1 -DREPETITIONS=10 -DXORSHIFT_SEED=2
#SIMFLAGS?=-DMODE_RECT -DLAYERS=10 -DTRIX_D=1 -DREPETITIONS=1 -DXORSHIFT_SEED=2
SIMFLAGS?=-DMODE_RECT -DLAYERS=100 -DTRIX_D=60 -DREPETITIONS=1 -DXORSHIFT_SEED=2
BIN_SUFFIX?=_make
RUN_ARG?=1

.SUFFIXES:

all: bin/sim_gcstrix${BIN_SUFFIX}

bin/sim_gcstrix${BIN_SUFFIX}: sim_gcstrix.c ${COMMON} Makefile
	${CC} ${CFLAGS} ${SIMFLAGS} $< -lm -o $@

.PHONY: run
run: bin/sim_gcstrix${BIN_SUFFIX}
	$< ${RUN_ARG}

.PHONY: clean
clean:
	rm bin/sim_gcstrix${BIN_SUFFIX}

.PHONY: say
say:
	@echo "SIMFLAGS is ${SIMFLAGS}"
