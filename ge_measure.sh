#!/bin/bash
#$ -S /bin/bash
#$ -N gcstrix_simulation
#$ -cwd
#$ -o /dev/null
#$ -e /dev/null
#$ -l h_vmem=512M,mem_free=512M
#$ -t 1-10

set -e

(

date

LAYERS=20
#TRIX_D=${SGE_TASK_ID]
TRIX_D=100
KREPETITIONS=1000
# 'DELAY' or 'SKEW' or 'DELTIME -DMID_AMOUNT=10' or 'HORZ' or 'MAXSKEW':
MODE_D="MAXSKEW"
# 'delays' or 'skews' or 'delti10' or 'horz' or 'maxskew':
MODE_STR=maxskew

XORSHIFT_SEED=${JOB_ID}
# At 1k layers: roughly 36 minutes for 200K simulations
FILENAME="logs/${MODE_STR}_v3_d${TRIX_D}_l${LAYERS}_r${KREPETITIONS}k_j${JOB_ID}_s${XORSHIFT_SEED}_x${SGE_TASK_ID}_uniqc.txt"
# no 'v': Old version, uses xorshift1024, drop 10, discards only the one top bit, uses rest.
# 'v2': Old version, uses xorshift1024, drop 10, discards top 8 bits and bottom 8 bits.
# 'v3': New version, uses xoshiro512starstar, drop 10, discards top 8 bits and bottom 8 bits.
echo "Output to ${FILENAME}"
BIN_SUFFIX="_${JOB_ID}_${SGE_TASK_ID}"
set -x
make -s -B SIMFLAGS="-DMODE_${MODE_D} -DLAYERS=${LAYERS} -DTRIX_D=${TRIX_D} -DREPETITIONS=${KREPETITIONS}000 -DXORSHIFT_SEED=${XORSHIFT_SEED}" BIN_SUFFIX="${BIN_SUFFIX}" RUN_ARG=${SGE_TASK_ID} run | ./sortuniqc.pl > ${FILENAME}
set +x
echo "Complete, deleting bin ..."
make BIN_SUFFIX="${BIN_SUFFIX}" clean
echo "Fully complete"
date

) > logs/out-$JOB_ID-$SGE_TASK_ID 2>&1
