#!/bin/bash

set -e

if [ $# -ne 2 ]
then
    echo "USAGE: $0 JOB_ID NUM_TASKS"
    exit 1
fi

if [ $1 != "$1" ] || [ $2 != "$2" ]
then
    echo "lolwut?!"
    exit 2
fi

for i in $(seq $2)
do
    JOB_ID=$1 SGE_TASK_ID=$i bash ./ge_measure.sh &
done

echo "All jobs started.  Waiting ..."
wait
