#include <assert.h>
#include <inttypes.h>
#include <math.h>  // round()
#include <stdio.h>
#include <stdint.h>

#include "splitmix64.h"
#include "xoshiro512starstar.h"

#include "splitmix64.c"
//#define XORSHIFT_PRINT_SEED
#include "xoshiro512starstar.c"


// === Set to 0 or 1 ===
// Select your favorite print result(s)
#define PRINT_VERT 0
#define PRINT_RECT 0

#if defined(MODE_SKEW)
#define PRINT_MAXSKEW 0
#define PRINT_FINALSKEW 1
#define PRINT_FINALDELAY 0
#define PRINT_MIDDELAY 0
#define PRINT_HORZ 0
#elif defined(MODE_DELAY)
#define PRINT_MAXSKEW 0
#define PRINT_FINALSKEW 0
#define PRINT_FINALDELAY 1
#define PRINT_MIDDELAY 0
#define PRINT_HORZ 0
#elif defined(MODE_DELTIME)
#define PRINT_MAXSKEW 0
#define PRINT_FINALSKEW 0
#define PRINT_FINALDELAY 1
#define PRINT_MIDDELAY 1
#define PRINT_HORZ 0
#ifndef MID_AMOUNT
#error "Must define MID_AMOUNT for PRINT_MIDDELAY"
#endif
#elif defined(MODE_HORZ)
#define PRINT_MAXSKEW 0
#define PRINT_FINALSKEW 0
#define PRINT_FINALDELAY 0
#define PRINT_MIDDELAY 0
#define PRINT_HORZ 1
#elif defined(MODE_MAXSKEW)
#define PRINT_MAXSKEW 1
#define PRINT_FINALSKEW 0
#define PRINT_FINALDELAY 0
#define PRINT_MIDDELAY 0
#define PRINT_HORZ 0
#elif defined(MODE_RECT)
#define PRINT_MAXSKEW 0
#define PRINT_FINALSKEW 0
#define PRINT_FINALDELAY 0
#define PRINT_MIDDELAY 0
#define PRINT_HORZ 0
#undef PRINT_RECT
#define PRINT_RECT 1
#else
#error "Must define MODE_DELAY or MODE_SKEW or MODE_DELTIME or MODE_HORZ or MODE_MAXSKEW"
#endif

// === Other config options ===
// Number of layers
//#define LAYERS 1000
#ifndef LAYERS
#error "Must provide LAYERS"
#endif
// Number of nodes in the "topmost", "last" layer
//#define X 2
#ifndef TRIX_D
#error "Must provide TRIX_D"
#endif
#define X ((TRIX_D)+1)
#define PRINT_RECT_SKIPLAYERS 0
//#define PRINT_RECT_SKIPLAYERS (LAYERS-40)
// 1 minute is roughly 20k reps of 1k layers - OUTDATED
#ifndef REPETITIONS
#error "Must provide REPETITIONS"
#endif
#ifndef XORSHIFT_SEED
#error "Must provide XORSHIFT_SEED"
#endif

#ifndef DELAY_UNCERTAINTY
#define DELAY_UNCERTAINTY (1.0)
#endif
#ifndef PARAM_DELTA
#define PARAM_DELTA (1.0)
#endif
#ifndef PARAM_THETA
#define PARAM_THETA (0.01)
#endif
#ifndef PARAM_LAMBDA
#define PARAM_LAMBDA (5.0)
#endif
#ifndef PARAM_FAST_LAMBDA
// PARAM_LAMBDA / (1 + µ)
#define PARAM_FAST_LAMBDA (4.9)
#endif
#ifndef PARAM_D
#define PARAM_D (0.0)
#endif

typedef float float_t;
#define PRN_FLOAT "%f"

// === Auto-deduced.  Do not change! ===
// Number of nodes initially
#define N ((X) + (LAYERS) * 2)

static float_t uniform_random() {
    // The low bits are low-entropy, but probably discarded anyway.
    return xoshiro512starstar_next() * 1.0 / UINT64_MAX;
}

static int is_fast_trigger(float_t to_max, float_t from_min) {
    (void)to_max; (void)from_min;// FIXME
    return 0;
}

static float_t compute_pulse_time(float_t h_min, float_t h_zero, float_t h_max) {
    // Sort and potentially jump h_zero
    if (h_min > h_max) { float_t tmp = h_max; h_max = h_min; h_min = tmp; }
    if (h_zero < h_min) {
        h_zero = fmaxf(h_zero, h_min - PARAM_DELTA);
    } else if (h_zero > h_max) {
        h_zero = fminf(h_zero, h_max + PARAM_DELTA);
    }

    if (is_fast_trigger(h_max - h_zero, h_zero - h_min)) {
        return h_zero + PARAM_FAST_LAMBDA - PARAM_D;
    } else {
        return h_zero + PARAM_LAMBDA - PARAM_D;
    }
}

static int run_single() {
    float_t wavefront[N];
    memset(wavefront, 0, sizeof(wavefront));
    int wavefront_len = N;
    float_t view[3];

    #if PRINT_VERT
    // vertical, first data point
    printf("0");
    #endif

    #if 1
    for (int i = 0; i < wavefront_len; i += 1) {
        // wavefront[i] = (i > (LAYERS + X/2)) * 32000;  // Slope
        wavefront[i] = 0; // Perfect initialization
    }
    #else
    wavefront[0] = 0;
    for (int i = 1; i < wavefront_len; i += 1) {
        wavefront[i] = wavefront[i - 1] + uniform_random() * PARAM_INIT_NOISE; // FIXME
    }
    #endif

    for (int layer = 0; layer < LAYERS; ++layer) {
        wavefront_len -= 2;
        for (int i = 0; i < wavefront_len; i += 1) {
            /* Randomize pulse arrival times */
            view[0] = wavefront[i + 0] + uniform_random() * DELAY_UNCERTAINTY;
            view[1] = wavefront[i + 1] + uniform_random() * DELAY_UNCERTAINTY;
            view[2] = wavefront[i + 2] + uniform_random() * DELAY_UNCERTAINTY;
            // As the algorithm is written in the paper, the wait-loop might be exited before the third pulse is received.
            // TODO: Set third pulse to +inf if it is too large (median(h…)+Lambda-d)

            // Compute the algorithm's output
            float_t rate = 1 + uniform_random() * PARAM_THETA;
            wavefront[i] = compute_pulse_time(view[0] * rate, view[1] * rate, view[2] * rate) / rate;

            #if PRINT_RECT
            if (layer >= PRINT_RECT_SKIPLAYERS) {
                if (LAYERS - layer - 1 <= i && i <= LAYERS - layer - 1 + X - 1) {
                    printf(PRN_FLOAT, wavefront[i]);
                    if (i == LAYERS - layer - 1 + X - 1) {  // Ugh!
                        printf("\n");
                    } else {
                        printf(",");
                    }
                }
            }
            #endif
        }

        // If values become too large (1e3?), re-gauge down to zero
        if (wavefront[0] > 100) {
            for (int i = 1; i < wavefront_len; i += 1) {
                wavefront[i] -= wavefront[0];
            }
            wavefront[0] = 0;
        }

        #if PRINT_VERT
        // vertical, non-first data points
        printf("," PRN_FLOAT, wavefront[LAYERS - layer - 1]);
        #endif
        #if PRINT_MIDDELAY
        if (layer == LAYERS - 1 - MID_AMOUNT) {
            printf(PRN_FLOAT ",", wavefront[LAYERS - layer - 1]);
        }
        #endif
    }
    #if PRINT_VERT
    printf("\n");
    #endif

    #if PRINT_HORZ
    for (int i = 0; i < wavefront_len; ++i) {
        printf(PRN_FLOAT, wavefront[i]);
        if (i == wavefront_len - 1) {
            printf("\n");
        } else {
            printf(",");
        }
    }
    #endif

    #if PRINT_FINALSKEW
    printf(PRN_FLOAT "\n", wavefront[wavefront_len - 1] - wavefront[0]);
    #endif
    #if PRINT_MAXSKEW
    {
        float_t skew_max = 0;
        for (int i = 0; i < wavefront_len - 1; ++i) {
            float_t skew_here;
            if (wavefront[i] > wavefront[i + 1]) {
                skew_here = wavefront[i] - wavefront[i + 1];
            } else {
                skew_here = wavefront[i + 1] - wavefront[i];
            }
            skew_max = fmaxf(skew_max, skew_here);
        }
        printf(PRN_FLOAT "\n", skew_max);
    }
    #endif

    #if PRINT_FINALDELAY
    printf(PRN_FLOAT "\n", wavefront[0]);
    #endif

    return 0;
}

int main(int argc, char** argv) {
    if (argc == 1) {
        /* All is good. */
        xoshiro512starstar_init64(XORSHIFT_SEED);
    } else {
        uint64_t seed;
        if (argc != 2 || !sscanf(argv[1], "%" SCNu64, &seed)) {
            printf("USAGE: %s [SKIP_NUM]", argv[0]);
            return 1;
        }
        xoshiro512starstar_init64(XORSHIFT_SEED + 1 + seed + (seed << 25));  // The fairest die roll
    }

    #if PRINT_FINALSKEW
    printf("PARAMS_finalskew,l,%" PRId32 ",d,%" PRId32 ",r,%" PRId32 ";", LAYERS, X - 1, REPETITIONS);
    #endif
    #if PRINT_MAXSKEW
    printf("PARAMS_maxskew,l,%" PRId32 ",d,%" PRId32 ",r,%" PRId32 ";", LAYERS, X - 1, REPETITIONS);
    #endif
    #if PRINT_FINALDELAY
    printf("PARAMS_finaldelay,l,%" PRId32 ",d,%" PRId32 ";", LAYERS, X - 1);
    #endif
    #if PRINT_HORZ
    printf("PARAMS_horz,l,%" PRId32 ",d,%" PRId32 ";", LAYERS, X - 1);
    #endif
    #if PRINT_MIDDELAY
    printf("PARAMS_middelay,l,%" PRId32 "-%" PRId32 ";", LAYERS, MID_AMOUNT);
    #endif
    #if PRINT_RECT
    printf("PARAMS_finaldelay,l,%" PRId32 ",d,%" PRId32 ";", LAYERS, X - 1);
    assert(!(PRINT_HORZ || PRINT_VERT));  // RECT interferes with all other.
    assert(!((LAYERS - PRINT_RECT_SKIPLAYERS) > 100 || X > 100));  // RECT produces a LOT of output.
    assert(REPETITIONS == 1);  // How to distinguish between repetitions?
    #endif
    printf("md," PRN_FLOAT ",mu," PRN_FLOAT ",delta," PRN_FLOAT ",theta," PRN_FLOAT ",lambda," PRN_FLOAT ",fastlambda," PRN_FLOAT "\n",
        PARAM_D, DELAY_UNCERTAINTY, PARAM_DELTA, PARAM_THETA, PARAM_LAMBDA, PARAM_FAST_LAMBDA);

    for (uint32_t i = 0; i < REPETITIONS; ++i) {
        run_single();
    }

    return 0;
}
