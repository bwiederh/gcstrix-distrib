#!/usr/bin/perl -w

use strict;

my %blah;

while(<>)
{
        $blah{$_} = ($blah{$_} || 0) + 1;
}

foreach (keys %blah)
{
        print "$blah{$_} $_";
}
