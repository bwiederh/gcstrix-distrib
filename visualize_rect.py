#!/usr/bin/env python3

from PIL import Image, ImageDraw
import re
import sys

WIDTH_PER_NODE = 15
HEIGHT_SCALE = 50
HEIGHT_RANGE = 15.0

FILL_COLORS = [128, 255]


def parse_preamble(fp):
    line = fp.readline()

    # Skip blank lines
    while line == '\n':
        line = fp.readline()

    line = line.strip()
    if not line.startswith('PARAMS_'):
        print('Unexpected line start?! >>{}<<'.format(line), file=sys.stderr)
        return

    match = re.match('.*[^A-Za-z0-9,;._-]', line)
    if match:
        print('Valid PARAMS found, but not a good filename: {}'.format(match), file=sys.stderr)
        return

    return line


def parse_line(line):
    parts = line.split(',')
    assert parts[-1] != ''
    return [float(p) for p in parts]


def x_for_node(i):
    return i * WIDTH_PER_NODE + WIDTH_PER_NODE / 2


def y_for_value(val):
    return (HEIGHT_RANGE / 2 - val) * HEIGHT_SCALE


def render_delays(delays):
    avg = sum(delays) / len(delays)
    normalized_delays = [d - avg for d in delays]

    effective_height = int(HEIGHT_RANGE * HEIGHT_SCALE)
    img = Image.new('L', (WIDTH_PER_NODE * len(delays), effective_height))
    d = ImageDraw.Draw(img)

    for i in range(-int(HEIGHT_RANGE), int(HEIGHT_RANGE)):
        y = y_for_value(i)
        d.line((0, y, 5, y), fill=FILL_COLORS[0])
    y = y_for_value(0)
    d.line((0, y, 5, y), fill=FILL_COLORS[1])

    prev_e = None
    for i, e in enumerate(normalized_delays):
        if prev_e is not None:
            x1 = x_for_node(i - 1)
            x2 = x_for_node(i)
            y1 = y_for_value(prev_e)
            y2 = y_for_value(e)
            d.line((x1, y1, x2, y2), fill=FILL_COLORS[i % len(FILL_COLORS)])
        prev_e = e
    return img


def run_on(fp):
    params_line = parse_preamble(fp)
    if params_line is None:
        return 1

    i = 0
    while True:
        line = fp.readline()
        if line == '':
            break
        delays = parse_line(line)
        if not delays:
            raise AssertionError(line)
        img = render_delays(delays)
        filename = 'delayrender_{}_img{:03}.gif'.format(params_line, i)
        img.save(filename)
        print('Written to {}'.format(filename))
        i += 1



def run():
    if len(sys.argv) == 1:
        exit(run_on(sys.stdin))
    elif len(sys.argv) == 2:
        with open(sys.argv[1], 'r') as fp:
            exit(run_on(fp))
    else:
        print('USAGE: {} [FILENAME]'.format(sys.argv[0]), file=sys.stderr)
        exit(1)


if __name__ == '__main__':
    run()
