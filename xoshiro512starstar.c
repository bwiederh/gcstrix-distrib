/*  Written in 2018 by David Blackman and Sebastiano Vigna (vigna@acm.org)

To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty.

See <http://creativecommons.org/publicdomain/zero/1.0/>. */

#include <stdint.h>
#include <string.h>

static uint64_t xoshiro512starstar_s[8];

/* BEGIN Modified, Ben Wiederhake 2019-07-25 */

#include "splitmix64.h"

void xoshiro512starstar_init64(uint64_t x) {
       #ifdef XORSHIFT_PRINT_SEED
       printf("Using seed %" PRIu64 "\n", x);
       #endif
       splitmix64_init(x);
       for(int i = 0; i < 8; i++) {
               xoshiro512starstar_s[i] = splitmix64_next();
       }
}

/* END Modified, Ben Wiederhake 2019-07-25 */

/* This is xoshiro512** 1.0, an all-purpose, rock-solid generator. It has
   excellent (about 1ns) speed, an increased state (512 bits) that is
   large enough for any parallel application, and it passes all tests we
   are aware of.

   For generating just floating-point numbers, xoshiro512+ is even faster.

   The state must be seeded so that it is not everywhere zero. If you have
   a 64-bit seed, we suggest to seed a splitmix64 generator and use its
   output to fill s. */

static inline uint64_t rotl(const uint64_t x, int k) {
	return (x << k) | (x >> (64 - k));
}


uint64_t xoshiro512starstar_next(void) {
	const uint64_t result_starstar = rotl(xoshiro512starstar_s[1] * 5, 7) * 9;

	const uint64_t t = xoshiro512starstar_s[1] << 11;

	xoshiro512starstar_s[2] ^= xoshiro512starstar_s[0];
	xoshiro512starstar_s[5] ^= xoshiro512starstar_s[1];
	xoshiro512starstar_s[1] ^= xoshiro512starstar_s[2];
	xoshiro512starstar_s[7] ^= xoshiro512starstar_s[3];
	xoshiro512starstar_s[3] ^= xoshiro512starstar_s[4];
	xoshiro512starstar_s[4] ^= xoshiro512starstar_s[5];
	xoshiro512starstar_s[0] ^= xoshiro512starstar_s[6];
	xoshiro512starstar_s[6] ^= xoshiro512starstar_s[7];

	xoshiro512starstar_s[6] ^= t;

	xoshiro512starstar_s[7] = rotl(xoshiro512starstar_s[7], 21);

	return result_starstar;
}


/* This is the jump function for the generator. It is equivalent
   to 2^256 calls to next(); it can be used to generate 2^256
   non-overlapping subsequences for parallel computations. */

/* void xoshiro512starstar_jump(void) {
	static const uint64_t JUMP[] = { 0x33ed89b6e7a353f9, 0x760083d7955323be, 0x2837f2fbb5f22fae, 0x4b8c5674d309511c, 0xb11ac47a7ba28c25, 0xf1be7667092bcc1c, 0x53851efdb6df0aaf, 0x1ebbc8b23eaf25db };

	uint64_t t[sizeof xoshiro512starstar_s / sizeof *xoshiro512starstar_s];
	memset(t, 0, sizeof t);
	for(int i = 0; i < sizeof JUMP / sizeof *JUMP; i++)
		for(int b = 0; b < 64; b++) {
			if (JUMP[i] & UINT64_C(1) << b)
				for(int w = 0; w < sizeof xoshiro512starstar_s / sizeof *xoshiro512starstar_s; w++)
					t[w] ^= xoshiro512starstar_s[w];
			next();
		}

	memcpy(xoshiro512starstar_s, t, sizeof xoshiro512starstar_s);
} */
