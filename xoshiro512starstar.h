#ifndef XOSHIRO512STARSTAR_H
#define XOSHIRO512STARSTAR_H

void xoshiro512starstar_init64(uint64_t x);
uint64_t xoshiro512starstar_next(void);
// void xoshiro512starstar_jump(void);

#endif
